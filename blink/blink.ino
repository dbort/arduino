#define RED_PIN 2
#define GREEN_PIN 3
#define BLUE_PIN 4

#define NUM_PINS 4

int pins[NUM_PINS] = {LED_BUILTIN, RED_PIN, GREEN_PIN, BLUE_PIN};

void setup() {
  for (int i = 0; i < NUM_PINS; i++) {
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], LOW);
  }
}

void loop() {
  int on = false;
  while (true) {
    for (int i = 0; i < NUM_PINS; i++) {
      digitalWrite(pins[i], on ? HIGH : LOW);
    }

    on = !on;
    delay(1000);
  }
}
