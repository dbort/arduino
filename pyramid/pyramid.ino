#include <TimerOne.h>

#define LOCAL_TRACE 0
#if LOCAL_TRACE
#define TRACE(x) Serial.print(x)
#else
#define TRACE(x) ((void)(x))
#endif

// A single LED: e.g., red or blue or green.
class Led {
public:
  Led(int pin) : pin_(pin) {
    // Turn off the LED to start.
    pinMode(pin_, OUTPUT);
    digitalWrite(pin_, LOW);
  }

  // Set the on time of the LED: 0 is off, kMaxCounter/2 is half brightness,
  // kMaxCounter is full on.
  void set_on_count(int on_count) {
    on_count_ = on_count;
  }

  // Call this to let the LED know how far it is through a single PWM
  // period. Assumes 0 <= |counter| <= kMaxCounter
  void PwmTick(int counter) {
    if (counter == on_count_) {
      // Off for the second part, but check this first in case on_count_
      // is zero.
      digitalWrite(pin_, LOW);
    } else if (counter == 0) {
      // On for the first part.
      digitalWrite(pin_, HIGH);
    }
  }

private:
  const int pin_;

  int on_count_ = 0; 
};

#define MIN(a, b) ((a) < (b) ? (a) : (b))


// An immutable RGB color, each value in [0,255].
struct Color {
  const unsigned char r;
  const unsigned char g;
  const unsigned char b;

  Color(unsigned char r, unsigned char g, unsigned char b)
      : r(r), g(g), b(b) {}

  // To/from an int.
  static Color FromInt(long unsigned int c) {
    return Color(c&0xff, (c>>8)&0xff, (c>>16)&0xff);
  }
  long unsigned int ToInt() const {
    return (long unsigned int)r
        | ((long unsigned int)g)<<8
        | ((long unsigned int)b)<<16;
  }

  bool operator==(const Color &o) const {
    return r == o.r && g == o.g && b == o.b;
  }

  bool operator!=(const Color &o) const {
    return !(*this == o);
  }

  // Returns a new Color where each value is multiplied by the
  // supplied value. Clamps to 255.
  Color ScaleBy(float ratio) const {
    return Color((unsigned char)MIN(r * ratio, 255),
                 (unsigned char)MIN(g * ratio, 255),
                 (unsigned char)MIN(b * ratio, 255));
  }

  void Dump() const {
    TRACE("Color(");
    TRACE(r);
    TRACE(", ");
    TRACE(g);
    TRACE(", ");
    TRACE(b);
    TRACE(")\n");
  }

  // TODO: Gamma correction, smarter scaling?
  // TODO: HSV conversions
};


// Three LEDs that can be mixed to produce a color.
class RgbLed {
public:
  RgbLed(int red_pin, int green_pin, int blue_pin)
      : red_(red_pin), green_(green_pin), blue_(blue_pin) {}

  void set_color(const Color& c) {
    Color lc = Color::FromInt(last_color_);
    if (lc != c) {
      TRACE("Old: ");
      lc.Dump();
      TRACE("New: ");
      c.Dump();
      last_color_ = c.ToInt();
      red_.set_on_count(c.r);
      green_.set_on_count(c.g);
      blue_.set_on_count(c.b);
    }
  }

  // Forwards the tick onto each of the component LEDs.
  void PwmTick(int counter) {
    red_.PwmTick(counter);
    green_.PwmTick(counter);
    blue_.PwmTick(counter);
  }

public:
  long unsigned int last_color_ = 0;
  Led red_;
  Led green_;
  Led blue_;
};


// An absolute time, in milliseconds.
typedef unsigned long int InstantMs;

// A length of time, in milliseconds. Can be negative.
typedef long int DurationMs;


// Animation base class.
class Animation {
public:
  virtual void Tick(InstantMs now) = 0;
};


class ColorCycle : public Animation {
public:
  ColorCycle(RgbLed* led, const Color& focus, DurationMs period)
      : led_(led), focus_(focus), period_(period) {}

  void Tick(InstantMs now) final {
    if (now >= end_time_) {
      // This stage is done.
      // Start with now, since it could be way past end_time_
      // when we're called.
      start_time_ = now;
      end_time_ = start_time_ + period_/2;
      rising_ = !rising_;
    }

    // How far along are we, from 0.0 (start) to 1.0 (end)?
    float time_fraction =  // [0, 1]
        (float)(now - start_time_) / (end_time_ - start_time_);
    if (!rising_) {
      // We're falling: the closer to end_time_, the closer to 0.0.
      time_fraction = 1.0 - time_fraction;
    }
    led_->set_color(focus_.ScaleBy(time_fraction));
  }

private:
  // The LED that we drive.
  RgbLed *const led_;
  // The color we should revolve around.
  const Color focus_;
  // How long we should take to repeat.
  const DurationMs period_;

  InstantMs start_time_ = 0;
  InstantMs end_time_ = 0;
  bool rising_ = false;
};


class Blink : public Animation {
public:
  Blink(RgbLed* led, const Color& focus, DurationMs period)
      : led_(led), focus_(focus), period_(period) {}

  void Tick(InstantMs now) final {
    if (now >= end_time_) {
      // This stage is done.
      // Start with now, since it could be way past end_time_
      // when we're called.
      start_time_ = now;
      end_time_ = start_time_ + period_/2;
      on_ = !on_;
      if (on_) {
        led_->set_color(focus_);
      } else {
        led_->set_color(Color::FromInt(0));
      }
    }
  }

private:
  // The LED that we drive.
  RgbLed *const led_;
  // The color we should revolve around.
  const Color focus_;
  // How long we should take to repeat.
  const DurationMs period_;

  InstantMs start_time_ = 0;
  InstantMs end_time_ = 0;
  bool on_ = false;
};


static constexpr int kNumLegs = 3;
RgbLed legs[kNumLegs] = {
  // Using pins 0 and 1 (Tx/Rx) seem to mess up the uploader,
  // at least when LEDs are attached. Start with pin 2.
  // Unfortunately there aren't enough PWMs to drive 9 leds,
  // so we'll just bit-bang all of them.
  RgbLed(2, 3, 4),
  RgbLed(5, 6, 7),
  RgbLed(8, 9, 10),
};

//Blink anim0(&legs[0], Color(64, 64, 64), 1000);
ColorCycle anim0(&legs[0], Color(127, 0, 0), 5000);
ColorCycle anim1(&legs[1], Color(0, 127, 0), 4000);
ColorCycle anim2(&legs[2], Color(0, 0, 127), 3000);

static constexpr int kNumAnims = 3;
Animation* anims[kNumAnims] = {
  &anim0,
  &anim1,
  &anim2,
};

static constexpr int kMaxCounter = 255;

void toggle_builtin() {
  static bool builtin_on = false;
  digitalWrite(LED_BUILTIN, builtin_on ? HIGH : LOW);
  builtin_on = !builtin_on;
}

void loop() {
  // Tick the animations if time has advanced enough to matter.
  static InstantMs last_now = 0;
  InstantMs now = millis();
  if (now >= last_now + 10) {
    for (int i = 0; i < kNumAnims; i++) {
      anims[i]->Tick(now);
    }
    last_now = now;
    if (now % 500 == 0) toggle_builtin();
  }
}

void pwm_tick_isr() {
  static int counter = 0;

  counter++;
  if (counter > kMaxCounter) {
    counter = 0;
  }
  for (int i = 0; i < kNumLegs; i++) {
    legs[i].PwmTick(counter);
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(9600);

  // Set up a timer to drive the PWM at 80Hz, with 255 ticks per cycle.
  Timer1.initialize(50 /*usec*/);
  Timer1.attachInterrupt(pwm_tick_isr);
}
