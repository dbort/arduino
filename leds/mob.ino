void blink_led_builtin(long int ms) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(ms);
    digitalWrite(LED_BUILTIN, LOW);
    delay(ms);
}

void panic(int n) {
  pinMode(LED_BUILTIN, OUTPUT);
  if (n <= 0) {
    n = 1;
  }
  while (true) {
    for (int i = 0; i < 8; i++) {
      blink_led_builtin(50);
    }
    delay(500);
    for (int i = 0; i < n; i++) {
      blink_led_builtin(250);
    }
    delay(500);
  }
}

typedef unsigned char Position;
const Position kPosMax = 255;

// An absolute time, in milliseconds.
typedef unsigned long int InstantMs;
typedef long int DurationMs;

class Mob {
public:
  Mob(byte r, byte g, byte b): r_(r), g_(g), b_(b) { }

  // Advance the time.
  void Tick(InstantMs now) {
    cur_time_ = now;

    if (cur_time_ >= end_time_) {
      // This stage is done.
      cur_pos_ = end_pos_;

      // Set up the next state.
      start_pos_ = cur_pos_;
      if (state_ == State::RESTING) {
        // Time to move. Pick a new position,
        // not too close to the current one.
        state_ = State::MOVING;
        do {
          end_pos_ = (Position) random(kPosMax + 1);
        } while (abs((long)end_pos_ - (long)start_pos_) < kPosMax/8);
      } else if (state_ == State::MOVING) {
        // Time to rest. Stay here.
        state_ = State::RESTING;
        end_pos_ = start_pos_;
      } else {
        panic(2);
      }

      // Random amount of time to get to end_pos_.
      // Note that we start with cur_time_, since it could be
      // way past end_time_.
      start_time_ = cur_time_;
      end_time_ = start_time_ + random(500, 2000);
    }

    if (state_ == State::MOVING) {
      // Move to where we need to be.

      // Clamp the time just in case.
      InstantMs t = cur_time_;
      if (t < start_time_) {
        t = start_time_;
      }
      if (t > end_time_) {
        t = end_time_;
      }

      // How far along are we, from 0.0 (start) to 1.0 (end)?
      float time_fraction =  // [0, 1]
          (float)(t - start_time_) / (end_time_ - start_time_);

      // How far we'll move during this entire state.
      int total_distance = end_pos_ - start_pos_;  // Can be negative

#if 0
      // We use the first 0..pi of a sine wave to ease in/out.
      float distance_fraction = sin(time_fraction * 3.14159);
#endif
      int new_pos = start_pos_ + (int)(time_fraction * total_distance);
      if (new_pos < 0) {
        new_pos = 0;
      } else if (new_pos > kPosMax) {
        new_pos = kPosMax;
      }
      cur_pos_ = new_pos;
    }
  }

  void Render(long leds[], unsigned num_leds) {
    // TODO: make this more evenly hit the first/last LEDs.
    unsigned i = cur_pos_ * num_leds / kPosMax;
    leds[i] = ((unsigned long)r_ << 16) |
        ((unsigned long)g_ << 8) |
        ((unsigned long)b_);
  }

private:
  enum class State {
    RESTING,
    MOVING,
  };

  byte r_;
  byte g_;
  byte b_;

  InstantMs cur_time_ = 0;
  InstantMs start_time_ = cur_time_;
  InstantMs end_time_ = cur_time_;

  Position cur_pos_ = kPosMax/2;
  Position start_pos_ = cur_pos_;
  Position end_pos_ = cur_pos_;
  State state_ = State::RESTING;
};

void mob_loop() {
  Mob r(0x10, 0, 0);
  Mob g(0, 0x10, 0);
  Mob b(0, 0, 0x10);
  Mob y(0x10, 0x10, 0);

  while (true) {
    unsigned long now = millis();
    r.Tick(now);
    g.Tick(now);
    b.Tick(now);
    y.Tick(now);
    long *leds = new_led_frame();
    r.Render(leds, STRIP_LENGTH);
    g.Render(leds, STRIP_LENGTH);
    b.Render(leds, STRIP_LENGTH);
    y.Render(leds, STRIP_LENGTH);
    post_frame();
    delay(100);
  }
}
